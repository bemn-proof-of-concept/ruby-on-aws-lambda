# Ruby on AWS Lambda
This repo is base on the following tutorial:
https://blog.francium.tech/deploying-ruby-to-aws-lambda-function-and-layers-a6c00b53a5d0

## What to do
We want to deploy a Ruby script to AWS Lambda, so that our Nifi Server can make a request (can be direct ssh or RESTful HTTP) on the Ruby script, accepting a event and return response on stdout.