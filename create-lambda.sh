export ACCNT_NO=`aws sts get-caller-identity | jq -r .Account`
export REGION=`aws configure get region`
export ZIP_FILE=function.zip

# create lambda function

zip $ZIP_FILE handler.rb

aws lambda create-function \
  --region $REGION \
  --function-name ruby_generate_sitemap_xml \
  --zip-file fileb://$ZIP_FILE \
  --runtime ruby2.5 \
  --role arn:aws:iam::$ACCNT_NO\:role/sample_role \
  --timeout 20 \
  --handler handler.main

###

## create ruby gem lambda layer
docker run --rm -it -v $PWD:/var/gem_build -w /var/gem_build lambci/lambda:build-ruby2.5 bundle install --path=.

## deploy gem package
export GEMFILE=gem_layer.zip
export 
zip -r $GEMFILE ./ruby/ -x ./ruby/2.5.0/cache/\*

aws lambda publish-layer-version \
         --layer-name ruby_gem \
         --region $REGION \
         --compatible-runtimes ruby2.5 \
         --zip-file fileb://$GEMFILE

sh update-lambda.sh

# ## update function
# zip $ZIP_FILE handler.rb
# aws lambda update-function-code \
#   --region $REGION \
#   --function-name ruby_generate_sitemap_xml \
#   --zip-file fileb://$ZIP_FILE

## configure lambda layer and gem path
export LAYER_ARN=`aws lambda list-layer-versions --region $REGION --layer-name ruby_gem | jq -r ".LayerVersions[0].LayerVersionArn"`

aws lambda update-function-configuration \
      --function-name ruby_generate_sitemap_xml\
      --region $REGION \
      --layers $LAYER_ARN \
      --environment '{"Variables": {"GEM_PATH":"/opt/ruby/2.5.0"}}'

