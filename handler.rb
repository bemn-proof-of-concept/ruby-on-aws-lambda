# require 'nokogiri'
# require 'aws-sdk'

# def main(event:, context:)
#     parser = Nokogiri::HTML('<html><body><div id =\'checkNokogiri\'>I am Parse Value<div/></body><html>')
# {
#    nokogiri_parsed_value: parser.css('#checkNokogiri').text
# }
# end

require 'json'
require 'xml-sitemap'

def main (event:, context:)
  create_xml(event)
end

def create_xml(raw_json)
# def create_xml(raw_filename)
#   raw_json = JSON.parse(File.read('./raw_json/' + raw_filename));
  map = XmlSitemap.new('nightly.financeasia.com')
  
  raw_json.each do |item|
    url = ''
    case item["ArticleType"]
    when 'News'
      id = item["pkCIID"]
      slug = item["Slug"]
      url = "/article/#{slug}/#{id}"
      map.add(url)
    else
    end
   #  map = XmlSitemap.map(url)
  end

  # Render the sitemap XML
  return map.render
end
