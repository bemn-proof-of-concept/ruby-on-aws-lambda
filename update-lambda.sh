export REGION=`aws configure get region`
export ZIP_FILE=function.zip

## update function
zip $ZIP_FILE handler.rb
aws lambda update-function-code \
  --region $REGION \
  --function-name ruby_generate_sitemap_xml \
  --zip-file fileb://$ZIP_FILE